package ru.t1.sarychevv.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.dto.response.AbstractResultResponse;

@NoArgsConstructor
public class UserLogoutResponse extends AbstractResultResponse {

    public UserLogoutResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
