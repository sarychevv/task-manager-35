package ru.t1.sarychevv.tm.dto.response;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.model.User;

public class AbstractUserResponse extends AbstractResultResponse {

    @Getter
    @Nullable
    private User user;

    @Getter
    @Nullable
    private String token;

    public AbstractUserResponse(@Nullable final String token) {
        this.token = token;
    }

    public AbstractUserResponse(@Nullable final User user) {
        this.user = user;
    }

    public AbstractUserResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
