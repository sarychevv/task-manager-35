package ru.t1.sarychevv.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.sarychevv.tm.api.repository.IProjectRepository;
import ru.t1.sarychevv.tm.enumerated.ProjectSort;
import ru.t1.sarychevv.tm.model.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProjectRepositoryTest {

    private static final Integer NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    private static final String TEST_NAME = "Test project";

    private static final String TEST_DESCRIPTION = "Test description";

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectRepository projectRepository;

    @Before
    public void initRepository() {
        projectList = new ArrayList<>();
        projectRepository = new ProjectRepository();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull Project project = new Project();
            project.setName(TEST_NAME + i);
            project.setDescription(TEST_DESCRIPTION + i);
            if (i <= 5) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            projectRepository.add(project);
            projectList.add(project);
        }
    }

    @Test
    public void testAdd() {
        Integer expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull Project project = new Project();
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectRepository.add(userId, project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
        @Nullable final Project actualProject = projectRepository.findOneById(userId, project.getId());
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(TEST_NAME, actualProject.getName());
        Assert.assertEquals(TEST_DESCRIPTION, actualProject.getDescription());
        Assert.assertEquals(userId, actualProject.getUserId());
    }

    @Test
    public void testAddProject() {
        Integer expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Project newProject = new Project();
        newProject.setName(TEST_NAME + expectedNumberOfEntries);
        newProject.setDescription(TEST_DESCRIPTION + expectedNumberOfEntries);
        projectRepository.add(newProject);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }


    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull final List<Project> projectList = new ArrayList<>();
        for (int i = 0; i < expectedNumberOfEntries; i++) {
            projectList.add(new Project());
        }
        projectRepository.add(projectList);
        Assert.assertEquals((long) expectedNumberOfEntries + NUMBER_OF_ENTRIES, (long)projectRepository.getSize());
    }

    @Test
    public void testSet() {
        final int recordsCount = NUMBER_OF_ENTRIES / 2;
        final Integer expectedNumberOfEntries = recordsCount;
        final int oldProjectSize = projectRepository.findAll().size();
        @NotNull final List<Project> projectList = new ArrayList<>();
        for (int i = 0; i < recordsCount; i++) {
            projectList.add(new Project());
        }
        projectRepository.set(projectList);
        final int newProjectSize = projectRepository.findAll().size();
        Assert.assertNotEquals(newProjectSize, oldProjectSize);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testExistsById() {
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String wrongId = "501";
        @NotNull Project project = new Project();
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectRepository.add(userId, project);
        Assert.assertEquals(true, projectRepository.existsById(project.getId()));
        Assert.assertEquals(false, projectRepository.existsById(wrongId));
    }

    @Test
    public void testExistsByIdForUser() {
        @NotNull final String wrongId = "501";
        @NotNull Project project = new Project();
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectRepository.add(USER_ID_1, project);
        Assert.assertEquals(true, projectRepository.existsById(USER_ID_1, project.getId()));
        Assert.assertEquals(false, projectRepository.existsById(USER_ID_2, project.getId()));
        Assert.assertEquals(false, projectRepository.existsById(USER_ID_1, wrongId));
        Assert.assertEquals(false, projectRepository.existsById(USER_ID_2, wrongId));
    }

    @Test
    public void testFindAll() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, (Integer) projectRepository.findAll().size());
    }

    @Test
    public void testFindAllWithComparator() {
        @NotNull final String sortType = "BY_STATUS";
        @Nullable final ProjectSort sort = ProjectSort.toSort(sortType);
        if (sort != null) {
            Assert.assertEquals(sortType, ProjectSort.BY_STATUS.toString());
            Assert.assertEquals(NUMBER_OF_ENTRIES, (Integer) projectRepository.findAll(sort.getComparator()).size());
        }
    }

    @Test
    public void testFindAllForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES - projectRepository.findAll(USER_ID_2).size(),
                            projectRepository.findAll(USER_ID_1).size());
    }

    @Test
    public void testFindAllForUserWithComparator() {
        @NotNull final String sortType = "BY_STATUS";
        @Nullable final ProjectSort sort = ProjectSort.toSort(sortType);
        if (sort != null) {
            @Nullable final List<Project> projectList = projectRepository.findAll(USER_ID_1, sort.getComparator());
            Assert.assertEquals(sortType, ProjectSort.BY_STATUS.toString());
            if (projectList == null) return;
            Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, projectList.size());
        }
    }

    @Test
    public void testFindOneById() {
        @NotNull final String wrongId = "501";
        @NotNull Project project = new Project();
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectRepository.add(USER_ID_1, project);
        Assert.assertEquals(project, projectRepository.findOneById(project.getId()));
        Assert.assertNotEquals(project, projectRepository.findOneById(wrongId));
    }

    @Test
    public void testFindOneByIdForUser() {
        @NotNull final String wrongId = "501";
        @NotNull Project project = new Project();
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectRepository.add(USER_ID_1, project);
        Assert.assertEquals(project, projectRepository.findOneById(USER_ID_1, project.getId()));
        Assert.assertNotEquals(project, projectRepository.findOneById(USER_ID_2, project.getId()));
        Assert.assertNotEquals(project, projectRepository.findOneById(USER_ID_1, wrongId));
        Assert.assertNotEquals(project, projectRepository.findOneById(USER_ID_2, wrongId));
    }

    @Test
    public void testFindOneByIndex() {
        @NotNull Project project = new Project();
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectRepository.add(USER_ID_1, project);
        Assert.assertEquals(project, projectRepository.findOneByIndex(NUMBER_OF_ENTRIES));
        Assert.assertNotEquals(project, projectRepository.findOneByIndex(NUMBER_OF_ENTRIES - 1));
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull Project project = new Project();
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectRepository.add(USER_ID_1, project);
        Assert.assertEquals(project,
                projectRepository.findOneByIndex(USER_ID_1, projectRepository.findAll(USER_ID_1).size()-1));
        Assert.assertNotEquals(project,
                projectRepository.findOneByIndex(USER_ID_2, projectRepository.findAll(USER_ID_2).size()-1));
        Assert.assertNotEquals(project,
                projectRepository.findOneByIndex(USER_ID_1, projectRepository.findAll(USER_ID_1).size()-2));
        Assert.assertNotEquals(project,
                projectRepository.findOneByIndex(USER_ID_2, projectRepository.findAll(USER_ID_2).size()-2));
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
        Assert.assertEquals(projectList.size(), projectRepository.getSize().intValue());
    }

    @Test
    public void testGetSizeForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, projectRepository.getSize(USER_ID_1));
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 - 1, projectRepository.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveOne() {
        @NotNull Project project = new Project();
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectRepository.add(project);
        Assert.assertEquals(project, projectRepository.removeOne(project));
        Assert.assertEquals(false, projectRepository.existsById(project.getId()));
        Assert.assertNull(projectRepository.findOneById(project.getId()));
    }

    @Test
    public void testRemoveOneForUser() {
        @NotNull Project project = new Project();
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectRepository.add(USER_ID_1, project);
        Assert.assertEquals(project, projectRepository.removeOne(USER_ID_1, project));
        Assert.assertNull(projectRepository.findOneById(USER_ID_1, project.getId()));
        Assert.assertNull(projectRepository.findOneById(project.getId()));
        Assert.assertEquals(false, projectRepository.existsById(project.getId()));
        Assert.assertEquals(false, projectRepository.existsById(USER_ID_1, project.getId()));
    }

    @Test
    public void testRemoveOneById() {
        @NotNull Project project = new Project();
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectRepository.add(project);
        Assert.assertEquals(project, projectRepository.removeOneById(project.getId()));
        Assert.assertEquals(false, projectRepository.existsById(project.getId()));
        Assert.assertNull(projectRepository.findOneById(project.getId()));
    }

    @Test
    public void testRemoveOneByIdForUser() {
        @NotNull Project project = new Project();
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectRepository.add(USER_ID_1, project);
        Assert.assertEquals(project, projectRepository.removeOneById(USER_ID_1, project.getId()));
        Assert.assertNull(projectRepository.findOneById(USER_ID_1, project.getId()));
        Assert.assertNull(projectRepository.findOneById(project.getId()));
        Assert.assertEquals(false, projectRepository.existsById(project.getId()));
        Assert.assertEquals(false, projectRepository.existsById(USER_ID_1, project.getId()));
    }

    @Test
    public void testRemoveOneByIndex() {
        @NotNull Project project = new Project();
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectRepository.add(project);
        Assert.assertEquals(project, projectRepository.removeOneByIndex(NUMBER_OF_ENTRIES));
        Assert.assertEquals(false, projectRepository.existsById(project.getId()));
        Assert.assertNull(projectRepository.findOneById(project.getId()));
    }

    @Test
    public void testRemoveOneByIndexForUser() {
        @NotNull Project project = new Project();
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectRepository.add(USER_ID_1, project);
        Assert.assertEquals(project, projectRepository.removeOneByIndex(USER_ID_1, NUMBER_OF_ENTRIES / 2 + 1));
        Assert.assertEquals(false, projectRepository.existsById(project.getId()));
        Assert.assertNull(projectRepository.findOneById(project.getId()));
    }

    @Test
    public void testRemoveAllForUser() {
        @NotNull final List<Project> emptyList = new ArrayList<>();
        projectRepository.removeAll(USER_ID_1);
        Assert.assertEquals(emptyList, projectRepository.findAll(USER_ID_1));
        Assert.assertNotEquals(emptyList, projectRepository.findAll(USER_ID_2));
    }

    @Test
    public void testRemoveAll() {
        final int expectedNumberOfEntries = 0;
        projectRepository.removeAll();
        Assert.assertEquals(expectedNumberOfEntries, (long) projectRepository.getSize());
    }

}
