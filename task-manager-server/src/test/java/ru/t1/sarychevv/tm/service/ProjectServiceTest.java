package ru.t1.sarychevv.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.sarychevv.tm.api.service.IProjectService;
import ru.t1.sarychevv.tm.enumerated.ProjectSort;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.exception.entity.ModelNotFoundException;
import ru.t1.sarychevv.tm.exception.entity.ProjectNotFoundException;
import ru.t1.sarychevv.tm.exception.field.*;
import ru.t1.sarychevv.tm.model.Project;
import ru.t1.sarychevv.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProjectServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    private static final String TEST_NAME = "Test project";

    private static final String TEST_DESCRIPTION = "Test description";

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectService projectService;

    @Before
    public void initRepository() {
        projectList = new ArrayList<>();
        projectService = new ProjectService(new ProjectRepository());
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName(TEST_NAME + i);
            project.setDescription(TEST_DESCRIPTION + i);
            if (i <= 5) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            projectService.add(project);
            projectList.add(project);
        }
    }

    @Test
    public void testCreate() {
        @Nullable final Project project = projectService.create(USER_ID_1, TEST_NAME);
        if (project == null) return;
        Assert.assertEquals(project, projectService.findOneById(project.getId()));
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, projectService.getSize().intValue());
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateWithoutName() {
        projectService.create(USER_ID_1, null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateWithoutUserId() {
        projectService.create(null, TEST_NAME, TEST_DESCRIPTION);
    }

    @Test
    public void testCreateWithDescription() {
        @Nullable final Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        Assert.assertEquals(project, projectService.findOneById(project.getId()));
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, projectService.getSize().intValue());
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateWithDescriptionWithoutName() {
        projectService.create(USER_ID_1, null, TEST_DESCRIPTION);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateWithDescriptionWithoutUserId() {
        projectService.create(null, TEST_NAME, TEST_DESCRIPTION);
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testCreateWithoutDescription() {
        projectService.create(USER_ID_1, TEST_NAME, null);
    }

    @Test
    public void testAdd() {
        @NotNull final Project project = new Project();
        projectService.add(project);
        Assert.assertEquals(true, projectService.existsById(project.getId()));
    }

    @Test(expected = ModelEmptyException.class)
    public void testAddWithoutProject() {
        @Nullable final Project project = null;
        projectService.add(project);
    }

    @Test
    public void testAddForUser() {
        @NotNull final Project project = new Project();
        project.setUserId(USER_ID_1);
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectService.add(project);
        Assert.assertEquals(true, projectService.existsById(project.getId()));
    }

    @Test
    public void testExistsById() {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        Assert.assertEquals(true, projectService.existsById(project.getId()));
    }

    @Test(expected = IdEmptyException.class)
    public void testExistsByIdWithoutId() {
        projectService.existsById(null);
    }

    @Test
    public void testExistsByIdForUser() {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        Assert.assertEquals(true, projectService.existsById(USER_ID_1,project.getId()));
    }

    @Test(expected = IdEmptyException.class)
    public void testExistsByIdForUserWithoutId() {
        projectService.existsById(USER_ID_1,null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testExistsByIdForUserWithoutUserId() {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        projectService.existsById(null,project.getId());
    }

    @Test
    public void testUpdateById () {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        @NotNull String testName = "Тестовое наименование";
        @NotNull String testDescription = "Тестовое наименование";
        projectService.updateById(USER_ID_1, project.getId(), testName, testDescription);
        Assert.assertEquals(testName, projectService.findOneById(project.getId()).getName());
        Assert.assertEquals(testDescription, projectService.findOneById(project.getId()).getDescription());
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateByIdWithoutId () {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        @NotNull String testName = "Тестовое наименование";
        @NotNull String testDescription = "Тестовое описание";
        projectService.updateById(USER_ID_1, null, testName, testDescription);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIdWithoutName () {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        @NotNull String testDescription = "Тестовое описание";
        projectService.updateById(USER_ID_1, project.getId(), null, testDescription);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIdWithoutUserId () {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        @NotNull String testName = "Тестовое наименование";
        @NotNull String testDescription = "Тестовое описание";
        projectService.updateById(null, project.getId(), testName, testDescription);
    }

    @Test(expected = ModelNotFoundException.class)
    public void testUpdateByIdWithWrongId () {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        @NotNull String testName = "Тестовое наименование";
        @NotNull String testDescription = "Тестовое описание";
        projectService.updateById(USER_ID_1, "501", testName, testDescription);
    }

    @Test
    public void testUpdateByIndex () {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        @NotNull String testName = "Тестовое наименование";
        @NotNull String testDescription = "Тестовое наименование";
        projectService.updateByIndex(USER_ID_1, NUMBER_OF_ENTRIES / 2 + 1, testName, testDescription);
        Assert.assertEquals(testName, projectService.findOneById(project.getId()).getName());
        Assert.assertEquals(testDescription, projectService.findOneById(project.getId()).getDescription());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIndexWithoutIndex () {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        @NotNull String testName = "Тестовое наименование";
        @NotNull String testDescription = "Тестовое описание";
        projectService.updateByIndex(USER_ID_1, null, testName, testDescription);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIndexWithoutName () {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        @NotNull String testDescription = "Тестовое описание";
        projectService.updateByIndex(USER_ID_1, NUMBER_OF_ENTRIES / 2 + 1, null, testDescription);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIndexWithoutUserId () {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        @NotNull String testName = "Тестовое наименование";
        @NotNull String testDescription = "Тестовое описание";
        projectService.updateByIndex(null, NUMBER_OF_ENTRIES / 2 + 1, testName, testDescription);
    }

    @Test
    public void testChangeStatusById() {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        projectService.changeStatusById(USER_ID_1, project.getId(), Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectService.findOneById(project.getId()).getStatus());
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeStatusByIdWithoutId() {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        projectService.changeStatusById(USER_ID_1, null, Status.COMPLETED);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeStatusByIdWithoutUserId() {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        projectService.changeStatusById(null, project.getId(), Status.COMPLETED);
    }

    @Test(expected = StatusEmptyException.class)
    public void testChangeStatusByIdWithoutStatus() {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        projectService.changeStatusById(USER_ID_1, project.getId(), null);
    }

    @Test
    public void testChangeStatusByIndex() {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        projectService.changeStatusByIndex(USER_ID_1, NUMBER_OF_ENTRIES / 2 + 1, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectService.findOneById(project.getId()).getStatus());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeStatusByIdWithoutIndex() {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        projectService.changeStatusByIndex(USER_ID_1, null, Status.COMPLETED);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeStatusByIdWithoutUserIndex() {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        projectService.changeStatusByIndex(null, NUMBER_OF_ENTRIES / 2 + 1, Status.COMPLETED);
    }

    @Test(expected = StatusEmptyException.class)
    public void testChangeStatusByIndexWithoutStatus() {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        projectService.changeStatusByIndex(USER_ID_1, NUMBER_OF_ENTRIES / 2 + 1, null);
    }

    @Test
    public void testFindAll() {
        Assert.assertEquals(projectService.getSize().intValue(), projectService.findAll().size());
    }

    @Test
    public void testFindAllForUser() {
        Assert.assertEquals(projectService.getSize().intValue()/2+1, projectService.findAll(USER_ID_1).size());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllForUserWithoutUserId() {
        @Nullable String testUserId = null;
        projectService.findAll(testUserId);
    }

    @Test
    public void testFindAllForUserWithComparator() {
        @NotNull final String sortType = "BY_STATUS";
        @Nullable final ProjectSort sort = ProjectSort.toSort(sortType);
        Assert.assertEquals(projectService.getSize().intValue()/2+1, projectService.findAll(USER_ID_1, sort.getComparator()).size());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllForUserWithComparatorWithoutUserId() {
        @NotNull final String sortType = "BY_STATUS";
        @Nullable final ProjectSort sort = ProjectSort.toSort(sortType);
        projectService.findAll(null, sort.getComparator());
    }

    @Test
    public void testFindAllForUserWithComparatorWithoutComparator() {
        @NotNull final String sortType = "BY_STATUS";
        @Nullable final ProjectSort sort = ProjectSort.toSort(sortType);
        Assert.assertEquals(projectService.getSize().intValue()/2+1, projectService.findAll(USER_ID_1, null).size());
    }

    @Test
    public void testFindOneById() {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        Assert.assertEquals(TEST_NAME, projectService.findOneById(project.getId()).getName());
        Assert.assertEquals(TEST_DESCRIPTION, projectService.findOneById(project.getId()).getDescription());
    }

    @Test
    public void testFindOneByIdForUser() {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        Assert.assertEquals(TEST_NAME, projectService.findOneById(USER_ID_1, project.getId()).getName());
        Assert.assertEquals(TEST_DESCRIPTION, projectService.findOneById(USER_ID_1, project.getId()).getDescription());
    }


    @Test(expected = UserIdEmptyException.class)
    public void testFindOneByIdForUserWithoutUserId() {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        projectService.findOneById(null, project.getId());
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectService.getSize().intValue());
    }

    @Test
    public void testGetSizeForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, projectService.getSize(USER_ID_1));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testGetSizeForUserWithoutUserId() {
        projectService.getSize(null);
    }
}
