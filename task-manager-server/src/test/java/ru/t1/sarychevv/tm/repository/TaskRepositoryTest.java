package ru.t1.sarychevv.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.sarychevv.tm.api.repository.IProjectRepository;
import ru.t1.sarychevv.tm.api.repository.ITaskRepository;
import ru.t1.sarychevv.tm.enumerated.TaskSort;
import ru.t1.sarychevv.tm.model.Project;
import ru.t1.sarychevv.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TaskRepositoryTest {

    private static final Integer NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    private static final String TEST_NAME = "Test task";

    private static final String TEST_DESCRIPTION = "Test description";

    @NotNull
    private List<Task> taskList;

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    private IProjectRepository projectRepository;

    @Before
    public void initRepository() {
        taskList = new ArrayList<>();
        taskRepository = new TaskRepository();
        projectRepository = new ProjectRepository();
        @NotNull Project project = new Project();
        projectRepository.add(project);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull Task task = new Task();
            task.setName(TEST_NAME + i);
            task.setDescription(TEST_DESCRIPTION + i);
            if (i <= 5) {
                task.setUserId(USER_ID_1);
                task.setProjectId(project.getId());
            }
            else {
                task.setUserId(USER_ID_2);
            }
            taskRepository.add(task);
            taskList.add(task);
        }
    }

    @Test
    public void findAllByProjectId() {
        Assert.assertNotNull(taskRepository.findAllByProjectId(USER_ID_1, projectRepository.findOneByIndex(0).getId()));
    }

    @Test
    public void testAdd() {
        Integer expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull Task task = new Task();
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskRepository.add(userId, task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
        @Nullable final Task actualTack = taskRepository.findOneById(userId, task.getId());
        Assert.assertNotNull(actualTack);
        Assert.assertEquals(TEST_NAME, actualTack.getName());
        Assert.assertEquals(TEST_DESCRIPTION, actualTack.getDescription());
        Assert.assertEquals(userId, actualTack.getUserId());
    }

    @Test
    public void testAddTask() {
        Integer expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Task newTask = new Task();
        newTask.setName(TEST_NAME + expectedNumberOfEntries);
        newTask.setDescription(TEST_DESCRIPTION + expectedNumberOfEntries);
        taskRepository.add(newTask);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }


    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull final List<Task> taskList = new ArrayList<>();
        for (int i = 0; i < expectedNumberOfEntries; i++) {
            taskList.add(new Task());
        }
        taskRepository.add(taskList);
        Assert.assertEquals((long) expectedNumberOfEntries + NUMBER_OF_ENTRIES, (long) taskRepository.getSize());
    }

    @Test
    public void testSet() {
        final int recordsCount = NUMBER_OF_ENTRIES / 2;
        final Integer expectedNumberOfEntries = recordsCount;
        final int oldTaskSize = taskRepository.findAll().size();
        @NotNull final List<Task> taskList = new ArrayList<>();
        for (int i = 0; i < recordsCount; i++) {
            taskList.add(new Task());
        }
        taskRepository.set(taskList);
        final int newTaskSize = taskRepository.findAll().size();
        Assert.assertNotEquals(newTaskSize, oldTaskSize);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testExistsById() {
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String wrongId = "501";
        @NotNull Task task = new Task();
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskRepository.add(userId, task);
        Assert.assertEquals(true, taskRepository.existsById(task.getId()));
        Assert.assertEquals(false, taskRepository.existsById(wrongId));
    }

    @Test
    public void testExistsByIdForUser() {
        @NotNull final String wrongId = "501";
        @NotNull Task task = new Task();
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskRepository.add(USER_ID_1, task);
        Assert.assertEquals(true, taskRepository.existsById(USER_ID_1, task.getId()));
        Assert.assertEquals(false, taskRepository.existsById(USER_ID_2, task.getId()));
        Assert.assertEquals(false, taskRepository.existsById(USER_ID_1, wrongId));
        Assert.assertEquals(false, taskRepository.existsById(USER_ID_2, wrongId));
    }

    @Test
    public void testFindAll() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, (Integer) taskRepository.findAll().size());
    }

    @Test
    public void testFindAllWithComparator() {
        @NotNull final String sortType = "BY_STATUS";
        @Nullable final TaskSort sort = TaskSort.toSort(sortType);
        if (sort != null) {
            Assert.assertEquals(sortType, TaskSort.BY_STATUS.toString());
            Assert.assertEquals(NUMBER_OF_ENTRIES, (Integer) taskRepository.findAll(sort.getComparator()).size());
        }
    }

    @Test
    public void testFindAllForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES - taskRepository.findAll(USER_ID_2).size(),
                taskRepository.findAll(USER_ID_1).size());
    }

    @Test
    public void testFindAllForUserWithComparator() {
        @NotNull final String sortType = "BY_STATUS";
        @Nullable final TaskSort sort = TaskSort.toSort(sortType);
        if (sort != null) {
            @Nullable final List<Task> taskList = taskRepository.findAll(USER_ID_1, sort.getComparator());
            Assert.assertEquals(sortType, TaskSort.BY_STATUS.toString());
            if (taskList == null) return;
            Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, taskList.size());
        }
    }

    @Test
    public void testFindOneById() {
        @NotNull final String wrongId = "501";
        @NotNull Task task = new Task();
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskRepository.add(USER_ID_1, task);
        Assert.assertEquals(task, taskRepository.findOneById(task.getId()));
        Assert.assertNotEquals(task, taskRepository.findOneById(wrongId));
    }

    @Test
    public void testFindOneByIdForUser() {
        @NotNull final String wrongId = "501";
        @NotNull Task task = new Task();
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskRepository.add(USER_ID_1, task);
        Assert.assertEquals(task, taskRepository.findOneById(USER_ID_1, task.getId()));
        Assert.assertNotEquals(task, taskRepository.findOneById(USER_ID_2, task.getId()));
        Assert.assertNotEquals(task, taskRepository.findOneById(USER_ID_1, wrongId));
        Assert.assertNotEquals(task, taskRepository.findOneById(USER_ID_2, wrongId));
    }

    @Test
    public void testFindOneByIndex() {
        @NotNull Task task = new Task();
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskRepository.add(USER_ID_1, task);
        Assert.assertEquals(task, taskRepository.findOneByIndex(NUMBER_OF_ENTRIES));
        Assert.assertNotEquals(task, taskRepository.findOneByIndex(NUMBER_OF_ENTRIES - 1));
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull Task task = new Task();
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskRepository.add(USER_ID_1, task);
        Assert.assertEquals(task,
                taskRepository.findOneByIndex(USER_ID_1, taskRepository.findAll(USER_ID_1).size()-1));
        Assert.assertNotEquals(task,
                taskRepository.findOneByIndex(USER_ID_2, taskRepository.findAll(USER_ID_2).size()-1));
        Assert.assertNotEquals(task,
                taskRepository.findOneByIndex(USER_ID_1, taskRepository.findAll(USER_ID_1).size()-2));
        Assert.assertNotEquals(task,
                taskRepository.findOneByIndex(USER_ID_2, taskRepository.findAll(USER_ID_2).size()-2));
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
        Assert.assertEquals(taskList.size(), taskRepository.getSize().intValue());
    }

    @Test
    public void testGetSizeForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, taskRepository.getSize(USER_ID_1));
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 - 1, taskRepository.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveOne() {
        @NotNull Task task = new Task();
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskRepository.add(task);
        Assert.assertEquals(task, taskRepository.removeOne(task));
        Assert.assertEquals(false, taskRepository.existsById(task.getId()));
        Assert.assertNull(taskRepository.findOneById(task.getId()));
    }

    @Test
    public void testRemoveOneForUser() {
        @NotNull Task task = new Task();
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskRepository.add(USER_ID_1, task);
        Assert.assertEquals(task, taskRepository.removeOne(USER_ID_1, task));
        Assert.assertNull(taskRepository.findOneById(USER_ID_1, task.getId()));
        Assert.assertNull(taskRepository.findOneById(task.getId()));
        Assert.assertEquals(false, taskRepository.existsById(task.getId()));
        Assert.assertEquals(false, taskRepository.existsById(USER_ID_1, task.getId()));
    }

    @Test
    public void testRemoveOneById() {
        @NotNull Task task = new Task();
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskRepository.add(task);
        Assert.assertEquals(task, taskRepository.removeOneById(task.getId()));
        Assert.assertEquals(false, taskRepository.existsById(task.getId()));
        Assert.assertNull(taskRepository.findOneById(task.getId()));
    }

    @Test
    public void testRemoveOneByIdForUser() {
        @NotNull Task task = new Task();
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskRepository.add(USER_ID_1, task);
        Assert.assertEquals(task, taskRepository.removeOneById(USER_ID_1, task.getId()));
        Assert.assertNull(taskRepository.findOneById(USER_ID_1, task.getId()));
        Assert.assertNull(taskRepository.findOneById(task.getId()));
        Assert.assertEquals(false, taskRepository.existsById(task.getId()));
        Assert.assertEquals(false, taskRepository.existsById(USER_ID_1, task.getId()));
    }

    @Test
    public void testRemoveOneByIndex() {
        @NotNull Task task = new Task();
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskRepository.add(task);
        Assert.assertEquals(task, taskRepository.removeOneByIndex(NUMBER_OF_ENTRIES));
        Assert.assertEquals(false, taskRepository.existsById(task.getId()));
        Assert.assertNull(taskRepository.findOneById(task.getId()));
    }

    @Test
    public void testRemoveOneByIndexForUser() {
        @NotNull Task task = new Task();
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskRepository.add(USER_ID_1, task);
        Assert.assertEquals(task, taskRepository.removeOneByIndex(USER_ID_1, NUMBER_OF_ENTRIES / 2 + 1));
        Assert.assertEquals(false, taskRepository.existsById(task.getId()));
        Assert.assertNull(taskRepository.findOneById(task.getId()));
    }

    @Test
    public void testRemoveAllForUser() {
        @NotNull final List<Task> emptyList = new ArrayList<>();
        taskRepository.removeAll(USER_ID_1);
        Assert.assertEquals(emptyList, taskRepository.findAll(USER_ID_1));
        Assert.assertNotEquals(emptyList, taskRepository.findAll(USER_ID_2));
    }

    @Test
    public void testRemoveAll() {
        final int expectedNumberOfEntries = 0;
        taskRepository.removeAll();
        Assert.assertEquals(expectedNumberOfEntries, (long) taskRepository.getSize());
    }
}
