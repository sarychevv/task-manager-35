package ru.t1.sarychevv.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.sarychevv.tm.api.repository.IUserRepository;
import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String TEST_LOGIN = "user";

    private static final String TEST_EMAIL = "user@yandex.ru";

    @NotNull
    private List<User> userList;

    @NotNull
    private IUserRepository userRepository;

    @Before
    public void initRepository() {
        userList = new ArrayList<>();
        userRepository = new UserRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull User user = new User();
            user.setLogin(TEST_LOGIN + i);
            user.setEmail("user" + i + "@yandex.ru");
            user.setRole(Role.USUAL);
            userRepository.add(user);
            userList.add(user);
        }
    }

    @Test
    public void testFindOneByLogin() {
        @NotNull User user = new User();
        user.setLogin(TEST_LOGIN);
        user.setEmail(TEST_EMAIL);
        userRepository.add(user);
        Assert.assertEquals(user, userRepository.findOneByLogin(TEST_LOGIN));
    }

    @Test
    public void testFindOneByEmail() {
        @NotNull User user = new User();
        user.setLogin(TEST_LOGIN);
        user.setEmail(TEST_EMAIL);
        userRepository.add(user);
        Assert.assertEquals(user, userRepository.findOneByEmail(TEST_EMAIL));
    }

    @Test
    public void testIsLoginExist() {
        @NotNull User user = new User();
        user.setLogin(TEST_LOGIN);
        user.setEmail(TEST_EMAIL);
        userRepository.add(user);
        Assert.assertEquals(true, userRepository.isLoginExist(TEST_LOGIN));
    }

    @Test
    public void testIsEmailExist() {
        @NotNull User user = new User();
        user.setLogin(TEST_LOGIN);
        user.setEmail(TEST_EMAIL);
        userRepository.add(user);
        Assert.assertEquals(true, userRepository.isEmailExist(TEST_EMAIL));
    }

    @Test
    public void testAdd() {
        Integer expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull User user = new User();
        user.setLogin(TEST_LOGIN);
        user.setEmail(TEST_EMAIL);
        userRepository.add(user);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
        @Nullable final User actualUser = userRepository.findOneById(user.getId());
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(TEST_LOGIN, actualUser.getLogin());
        Assert.assertEquals(TEST_EMAIL, actualUser.getEmail());
    }

    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull final List<User> userList = new ArrayList<>();
        for (int i = 0; i < expectedNumberOfEntries; i++) {
            userList.add(new User());
        }
        userRepository.add(userList);
        Assert.assertEquals((long) expectedNumberOfEntries + NUMBER_OF_ENTRIES,
                                    (long)userRepository.getSize());
    }

    @Test
    public void testSet() {
        final int recordsCount = NUMBER_OF_ENTRIES / 2;
        final Integer expectedNumberOfEntries = recordsCount;
        final int oldUserSize = userRepository.findAll().size();
        @NotNull final List<User> userList = new ArrayList<>();
        for (int i = 0; i < recordsCount; i++) {
            userList.add(new User());
        }
        userRepository.set(userList);
        Assert.assertNotEquals(userRepository.findAll().size(), oldUserSize);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testFindAll() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.findAll().size());
    }

    @Test
    public void testFindOneById() {
        @NotNull User user = new User();
        @NotNull final String wrongId = "501";
        user.setLogin(TEST_LOGIN);
        user.setEmail(TEST_EMAIL);
        userRepository.add(user);
        Assert.assertEquals(user, userRepository.findOneById(user.getId()));
        Assert.assertNotEquals(user, userRepository.findOneById(wrongId));
    }

    @Test
    public void testFindOneByIndex() {
        @NotNull User user = new User();
        user.setLogin(TEST_LOGIN);
        user.setEmail(TEST_EMAIL);
        userRepository.add(user);
        Assert.assertEquals(user, userRepository.findOneByIndex(NUMBER_OF_ENTRIES));
        Assert.assertNotEquals(user, userRepository.findOneByIndex(NUMBER_OF_ENTRIES - 1));
    }

    @Test
    public void testExistsById() {
        @NotNull User user = new User();
        @NotNull final String wrongId = "501";
        user.setLogin(TEST_LOGIN);
        user.setEmail(TEST_EMAIL);
        userRepository.add(user);
        Assert.assertEquals(true, userRepository.existsById(user.getId()));
        Assert.assertEquals(false, userRepository.existsById(wrongId));
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize().intValue());
        Assert.assertEquals(userList.size(), userRepository.getSize().intValue());
    }

    @Test
    public void testRemoveOne() {
        @NotNull User user = new User();
        user.setLogin(TEST_LOGIN);
        user.setEmail(TEST_EMAIL);
        userRepository.add(user);
        Assert.assertEquals(user, userRepository.removeOne(user));
        Assert.assertEquals(false, userRepository.existsById(user.getId()));
        Assert.assertNull(userRepository.findOneById(user.getId()));
    }

    @Test
    public void testRemoveOneById() {
        @NotNull User user = new User();
        user.setLogin(TEST_LOGIN);
        user.setEmail(TEST_EMAIL);
        userRepository.add(user);
        Assert.assertEquals(user, userRepository.removeOneById(user.getId()));
        Assert.assertEquals(false, userRepository.existsById(user.getId()));
        Assert.assertNull(userRepository.findOneById(user.getId()));
    }

    @Test
    public void testRemoveOneByIndex() {
        @NotNull User user = new User();
        user.setLogin(TEST_LOGIN);
        user.setEmail(TEST_EMAIL);
        userRepository.add(user);
        Assert.assertEquals(user, userRepository.removeOneByIndex(NUMBER_OF_ENTRIES));
        Assert.assertEquals(false, userRepository.existsById(user.getId()));
        Assert.assertNull(userRepository.findOneById(user.getId()));
    }
}
