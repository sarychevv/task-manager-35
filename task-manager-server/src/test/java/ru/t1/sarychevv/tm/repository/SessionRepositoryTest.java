package ru.t1.sarychevv.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.sarychevv.tm.api.repository.ISessionRepository;
import ru.t1.sarychevv.tm.enumerated.ProjectSort;
import ru.t1.sarychevv.tm.model.Project;
import ru.t1.sarychevv.tm.model.Session;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SessionRepositoryTest {

    private static final Integer NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    private static final String TEST_NAME = "Test session";

    private static final String TEST_DESCRIPTION = "Test description";

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private ISessionRepository sessionRepository;

    @Before
    public void initRepository() {
        sessionList = new ArrayList<>();
        sessionRepository = new SessionRepository();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull Session session = new Session();
            session.setName(TEST_NAME + i);
            session.setDescription("description" + i);
            if (i <= 5) session.setUserId(USER_ID_1);
            else session.setUserId(USER_ID_2);
            sessionRepository.add(session);
            sessionList.add(session);
        }
    }

    @Test
    public void testAdd() {
        Integer expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull Session session = new Session();
        session.setName(TEST_NAME);
        session.setDescription(TEST_DESCRIPTION);
        sessionRepository.add(userId, session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
        @Nullable final Session actualSession = sessionRepository.findOneById(userId, session.getId());
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(TEST_NAME, actualSession.getName());
        Assert.assertEquals(TEST_DESCRIPTION, actualSession.getDescription());
        Assert.assertEquals(userId, actualSession.getUserId());
    }

    @Test
    public void testAddSession() {
        Integer expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Session newSession = new Session();
        newSession.setName(TEST_NAME + expectedNumberOfEntries);
        newSession.setDescription(TEST_DESCRIPTION + expectedNumberOfEntries);
        sessionRepository.add(newSession);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }


    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull final List<Session> sessionList = new ArrayList<>();
        for (int i = 0; i < expectedNumberOfEntries; i++) {
            sessionList.add(new Session());
        }
        sessionRepository.add(sessionList);
        Assert.assertEquals((long) expectedNumberOfEntries + NUMBER_OF_ENTRIES, (long) sessionRepository.getSize());
    }

    @Test
    public void testSet() {
        final int recordsCount = NUMBER_OF_ENTRIES / 2;
        final Integer expectedNumberOfEntries = recordsCount;
        final int oldSessionSize = sessionRepository.findAll().size();
        @NotNull final List<Session> sessionList = new ArrayList<>();
        for (int i = 0; i < recordsCount; i++) {
            sessionList.add(new Session());
        }
        sessionRepository.set(sessionList);
        final int newSessionSize = sessionRepository.findAll().size();
        Assert.assertNotEquals(newSessionSize, oldSessionSize);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testExistsById() {
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String wrongId = "501";
        @NotNull Session session = new Session();
        session.setName(TEST_NAME);
        session.setDescription(TEST_DESCRIPTION);
        sessionRepository.add(userId, session);
        Assert.assertEquals(true, sessionRepository.existsById(session.getId()));
        Assert.assertEquals(false, sessionRepository.existsById(wrongId));
    }

    @Test
    public void testExistsByIdForUser() {
        @NotNull final String wrongId = "501";
        @NotNull Session session = new Session();
        session.setName(TEST_NAME);
        session.setDescription(TEST_DESCRIPTION);
        sessionRepository.add(USER_ID_1, session);
        Assert.assertEquals(true, sessionRepository.existsById(USER_ID_1, session.getId()));
        Assert.assertEquals(false, sessionRepository.existsById(USER_ID_2, session.getId()));
        Assert.assertEquals(false, sessionRepository.existsById(USER_ID_1, wrongId));
        Assert.assertEquals(false, sessionRepository.existsById(USER_ID_2, wrongId));
    }

    @Test
    public void testFindAll() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, (Integer) sessionRepository.findAll().size());
    }

    @Test
    public void testFindAllForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES - sessionRepository.findAll(USER_ID_2).size(),
                sessionRepository.findAll(USER_ID_1).size());
    }

    @Test
    public void testFindOneById() {
        @NotNull final String wrongId = "501";
        @NotNull Session session = new Session();
        session.setName(TEST_NAME);
        session.setDescription(TEST_DESCRIPTION);
        sessionRepository.add(USER_ID_1, session);
        Assert.assertEquals(session, sessionRepository.findOneById(session.getId()));
        Assert.assertNotEquals(session, sessionRepository.findOneById(wrongId));
    }

    @Test
    public void testFindOneByIdForUser() {
        @NotNull final String wrongId = "501";
        @NotNull Session session = new Session();
        session.setName(TEST_NAME);
        session.setDescription(TEST_DESCRIPTION);
        sessionRepository.add(USER_ID_1, session);
        Assert.assertEquals(session, sessionRepository.findOneById(USER_ID_1, session.getId()));
        Assert.assertNotEquals(session, sessionRepository.findOneById(USER_ID_2, session.getId()));
        Assert.assertNotEquals(session, sessionRepository.findOneById(USER_ID_1, wrongId));
        Assert.assertNotEquals(session, sessionRepository.findOneById(USER_ID_2, wrongId));
    }

    @Test
    public void testFindOneByIndex() {
        @NotNull Session session = new Session();
        session.setName(TEST_NAME);
        session.setDescription(TEST_DESCRIPTION);
        sessionRepository.add(USER_ID_1, session);
        Assert.assertEquals(session, sessionRepository.findOneByIndex(NUMBER_OF_ENTRIES));
        Assert.assertNotEquals(session, sessionRepository.findOneByIndex(NUMBER_OF_ENTRIES - 1));
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull Session session = new Session();
        session.setName(TEST_NAME);
        session.setDescription(TEST_DESCRIPTION);
        sessionRepository.add(USER_ID_1, session);
        Assert.assertEquals(session,
                sessionRepository.findOneByIndex(USER_ID_1, sessionRepository.findAll(USER_ID_1).size()-1));
        Assert.assertNotEquals(session,
                sessionRepository.findOneByIndex(USER_ID_2, sessionRepository.findAll(USER_ID_2).size()-1));
        Assert.assertNotEquals(session,
                sessionRepository.findOneByIndex(USER_ID_1, sessionRepository.findAll(USER_ID_1).size()-2));
        Assert.assertNotEquals(session,
                sessionRepository.findOneByIndex(USER_ID_2, sessionRepository.findAll(USER_ID_2).size()-2));
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
        Assert.assertEquals(sessionList.size(), sessionRepository.getSize().intValue());
    }

    @Test
    public void testGetSizeForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, sessionRepository.getSize(USER_ID_1));
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 - 1, sessionRepository.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveOne() {
        @NotNull Session session = new Session();
        session.setName(TEST_NAME);
        session.setDescription(TEST_DESCRIPTION);
        sessionRepository.add(session);
        Assert.assertEquals(session, sessionRepository.removeOne(session));
        Assert.assertEquals(false, sessionRepository.existsById(session.getId()));
        Assert.assertNull(sessionRepository.findOneById(session.getId()));
    }

    @Test
    public void testRemoveOneForUser() {
        @NotNull Session session = new Session();
        session.setName(TEST_NAME);
        session.setDescription(TEST_DESCRIPTION);
        sessionRepository.add(USER_ID_1, session);
        Assert.assertEquals(session, sessionRepository.removeOne(USER_ID_1, session));
        Assert.assertNull(sessionRepository.findOneById(USER_ID_1, session.getId()));
        Assert.assertNull(sessionRepository.findOneById(session.getId()));
        Assert.assertEquals(false, sessionRepository.existsById(session.getId()));
        Assert.assertEquals(false, sessionRepository.existsById(USER_ID_1, session.getId()));
    }

    @Test
    public void testRemoveOneById() {
        @NotNull Session session = new Session();
        session.setName(TEST_NAME);
        session.setDescription(TEST_DESCRIPTION);
        sessionRepository.add(session);
        Assert.assertEquals(session, sessionRepository.removeOneById(session.getId()));
        Assert.assertEquals(false, sessionRepository.existsById(session.getId()));
        Assert.assertNull(sessionRepository.findOneById(session.getId()));
    }

    @Test
    public void testRemoveOneByIdForUser() {
        @NotNull Session session = new Session();
        session.setName(TEST_NAME);
        session.setDescription(TEST_DESCRIPTION);
        sessionRepository.add(USER_ID_1, session);
        Assert.assertEquals(session, sessionRepository.removeOneById(USER_ID_1, session.getId()));
        Assert.assertNull(sessionRepository.findOneById(USER_ID_1, session.getId()));
        Assert.assertNull(sessionRepository.findOneById(session.getId()));
        Assert.assertEquals(false, sessionRepository.existsById(session.getId()));
        Assert.assertEquals(false, sessionRepository.existsById(USER_ID_1, session.getId()));
    }

    @Test
    public void testRemoveOneByIndex() {
        @NotNull Session session = new Session();
        session.setName(TEST_NAME);
        session.setDescription(TEST_DESCRIPTION);
        sessionRepository.add(session);
        Assert.assertEquals(session, sessionRepository.removeOneByIndex(NUMBER_OF_ENTRIES));
        Assert.assertEquals(false, sessionRepository.existsById(session.getId()));
        Assert.assertNull(sessionRepository.findOneById(session.getId()));
    }

    @Test
    public void testRemoveOneByIndexForUser() {
        @NotNull Session session = new Session();
        session.setName(TEST_NAME);
        session.setDescription(TEST_DESCRIPTION);
        sessionRepository.add(USER_ID_1, session);
        Assert.assertEquals(session, sessionRepository.removeOneByIndex(USER_ID_1, NUMBER_OF_ENTRIES / 2 + 1));
        Assert.assertEquals(false, sessionRepository.existsById(session.getId()));
        Assert.assertNull(sessionRepository.findOneById(session.getId()));
    }

    @Test
    public void testRemoveAllForUser() {
        @NotNull final List<Session> emptyList = new ArrayList<>();
        sessionRepository.removeAll(USER_ID_1);
        Assert.assertEquals(emptyList, sessionRepository.findAll(USER_ID_1));
        Assert.assertNotEquals(emptyList, sessionRepository.findAll(USER_ID_2));
    }

    @Test
    public void testRemoveAll() {
        final int expectedNumberOfEntries = 0;
        sessionRepository.removeAll();
        Assert.assertEquals(expectedNumberOfEntries, (long) sessionRepository.getSize());
    }

}
