package ru.t1.sarychevv.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.sarychevv.tm.api.service.IUserService;
import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.exception.field.*;
import ru.t1.sarychevv.tm.exception.user.ExistsLoginException;
import ru.t1.sarychevv.tm.exception.user.UserNotFoundException;
import ru.t1.sarychevv.tm.model.User;
import ru.t1.sarychevv.tm.repository.ProjectRepository;
import ru.t1.sarychevv.tm.repository.TaskRepository;
import ru.t1.sarychevv.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

public class UserServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String TEST_LOGIN = "Test login";

    private static final String TEST_PASSWORD = "Test password";

    private static final String TEST_EMAIL = "Test email";

    private static final String TEST_FIRST_NAME = "Test first name";

    private static final String TEST_LAST_NAME = "Test last name";

    private static final String TEST_MIDDLE_NAME = "Test middle name";

    @NotNull
    private List<User> userList;

    @NotNull
    private IUserService userService;

    @Before
    public void initRepository() {
        userList = new ArrayList<>();
        userService = new UserService(new PropertyService(), new UserRepository(),
                                      new ProjectRepository(), new TaskRepository());

        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("user" + i);
            user.setEmail("user@" + i + ".ru");
            user.setRole(Role.USUAL);
            userService.add(user);
            userList.add(user);
        }
    }

    @Test
    public void testCreate() {
        @Nullable final User user = userService.create(TEST_LOGIN, TEST_PASSWORD);
        Assert.assertEquals(user, userService.findOneById(user.getId()));
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, userService.getSize().intValue());
    }

    @Test
    public void testCreateWithEmail() {
        @Nullable final User user = userService.create(TEST_LOGIN, TEST_PASSWORD, TEST_EMAIL);
        Assert.assertEquals(user, userService.findOneById(user.getId()));
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, userService.getSize().intValue());
    }

    @Test
    public void testCreateWithRole() {
        @Nullable final User user = userService.create(TEST_LOGIN, TEST_PASSWORD, Role.ADMIN);
        Assert.assertEquals(user, userService.findOneById(user.getId()));
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, userService.getSize().intValue());
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test(expected = LoginEmptyException.class)
    public void testCreateWithoutLogin() {
        userService.create(null, TEST_PASSWORD);
    }

    @Test(expected = ExistsLoginException.class)
    public void testCreateLoginExists() {
        userService.create(TEST_LOGIN, TEST_PASSWORD);
        userService.create(TEST_LOGIN, TEST_PASSWORD);
    }

    @Test(expected = PasswordEmptyException.class)
    public void testCreateWithoutPassword() {
        userService.create(TEST_LOGIN, null);
    }

    @Test(expected = EmailEmptyException.class)
    public void testCreateWithoutEmail() {
        userService.create(TEST_LOGIN, TEST_PASSWORD, (String) null);
    }

    @Test(expected = RoleEmptyException.class)
    public void testCreateWithoutRole() {
        userService.create(TEST_LOGIN, TEST_PASSWORD, (Role) null);
    }

    @Test
    public void testFindByLogin() {
        Assert.assertEquals(userList.get(1), userService.findByLogin("user2"));
        Assert.assertEquals(userList.get(0), userService.findByLogin("user1"));
    }

    @Test(expected = LoginEmptyException.class)
    public void testFindByLoginWithoutLogin() {
        Assert.assertEquals(userList.get(0), userService.findByLogin(null));
    }

    @Test
    public void testRemoveOne() {
        Assert.assertEquals(userList.get(1), userService.removeOne(userList.get(1)));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, userService.getSize().intValue());
    }

    @Test
    public void testRemoveByLogin() {
        Assert.assertEquals(userList.get(1), userService.removeByLogin(userList.get(1).getLogin()));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, userService.getSize().intValue());
    }

    @Test(expected = LoginEmptyException.class)
    public void testRemoveByLoginWithoutLogin() {
        userService.removeByLogin(null);
    }

    @Test
    public void testSetPassword() {
        Assert.assertNotNull(userService.setPassword(userList.get(0).getId(), TEST_PASSWORD));
    }

    @Test(expected = IdEmptyException.class)
    public void testSetPasswordWithoutId() {
        userService.setPassword(null, TEST_PASSWORD);
    }

    @Test(expected = PasswordEmptyException.class)
    public void testSetPasswordWithoutPassword() {
        userService.setPassword(userList.get(1).getId(), null);
    }

    @Test
    public void testUpdateUser() {
        @NotNull final String id = userList.get(0).getId();
        userService.updateUser(id, TEST_FIRST_NAME, TEST_LAST_NAME, TEST_MIDDLE_NAME);
        @Nullable final User user = userService.findOneById(id);
        if (user == null) return;
        Assert.assertEquals(TEST_FIRST_NAME, user.getFirstName());
        Assert.assertEquals(TEST_LAST_NAME, user.getLastName());
        Assert.assertEquals(TEST_MIDDLE_NAME, user.getMiddleName());
    }

    @Test(expected = UserNotFoundException.class)
    public void testUpdateUserWithWrongId() {
        userService.updateUser("501", TEST_FIRST_NAME, TEST_LAST_NAME, TEST_MIDDLE_NAME);
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateUserWithoutId(
    ) {
        userService.updateUser(null, TEST_FIRST_NAME, TEST_LAST_NAME, TEST_MIDDLE_NAME);
    }

    @Test
    public void testIsLoginExist() {
        Assert.assertTrue(userService.isLoginExist("user4"));
    }

    @Test
    public void testIsEmailExist() {
        Assert.assertTrue(userService.isEmailExist("user@4.ru"));
    }

    @Test
    public void testLockUserByLogin() {
        @NotNull final User user = userService.lockUserByLogin(userList.get(0).getLogin());
        Assert.assertTrue(user.getLocked());
    }

    @Test(expected = LoginEmptyException.class)
    public void testLockUserByLoginWithoutLogin() {
        userService.lockUserByLogin(null);
    }

    @Test(expected = UserNotFoundException.class)
    public void testLockUserByIdWithoutId() {
        userService.lockUserByLogin("501");
    }

    @Test
    public void testUnlockUserByLogin() {
        @NotNull final User user = userService.unlockUserByLogin(userList.get(0).getLogin());
        Assert.assertFalse(user.getLocked());
    }

    @Test(expected = LoginEmptyException.class)
    public void testUnlockUserByLoginWithoutLogin() {
        userService.unlockUserByLogin(null);
    }

    @Test
    public void testFindOneById() {
        Assert.assertNotNull(userService.findOneById(userList.get(0).getId()));
    }

}
