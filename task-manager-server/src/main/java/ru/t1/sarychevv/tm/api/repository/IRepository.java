package ru.t1.sarychevv.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(@NotNull M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull Integer index);

    void removeAll();

    @Nullable
    M removeOne(@NotNull M model);

    @Nullable
    M removeOneById(@NotNull String id);

    @Nullable
    M removeOneByIndex(@NotNull Integer index);

    @NotNull
    Integer getSize();

    @NotNull
    Boolean existsById(@NotNull String id);

}
