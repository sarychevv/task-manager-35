package ru.t1.sarychevv.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.request.data.DataXmlSaveJaxBRequest;

public final class DataXmlSaveJaxBCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data in xml file";
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-xml-jaxb";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE XML]");
        serviceLocator.getDomainEndpoint().saveDataXmlJaxB(new DataXmlSaveJaxBRequest(getToken()));
    }

}
